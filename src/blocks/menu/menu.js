$('.menu__btn').click(function(e) {
  e.preventDefault();
  $(this).toggleClass('menu__btn--active');
  $('.menu__content').toggleClass('menu__content--active');
});

$('.menu__link--anchor').click(function(e) {
  e.preventDefault();
  $('.menu__content').removeClass('menu__content--active');
  $('.menu__btn').removeClass('menu__btn--active');
  $('html, body').animate({
    scrollTop: $($.attr(this, 'href')).offset().top
  }, 500);
});